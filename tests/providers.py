import random
from faker.providers import BaseProvider


class CustomSubmissionProvider(BaseProvider):
    word_list = (
        'a', 'ab', 'accusamus', 'accusantium', 'ad', 'adipisci', 'alias',
        'aliquam', 'aliquid', 'amet', 'animi', 'aperiam', 'architecto',
        'asperiores', 'aspernatur', 'assumenda', 'at', 'atque', 'aut', 'autem',
        'beatae', 'blanditiis', 'commodi', 'consectetur', 'consequatur',
        'consequuntur', 'corporis', 'corrupti', 'culpa', 'cum', 'cumque',
        'cupiditate', 'debitis', 'delectus', 'deleniti', 'deserunt', 'dicta',
        'dignissimos', 'distinctio', 'dolor', 'dolore', 'dolorem', 'doloremque',
        'dolores', 'doloribus', 'dolorum', 'ducimus', 'ea', 'eaque', 'earum',
        'eius', 'eligendi', 'enim', 'eos', 'error', 'esse', 'est', 'et', 'eum',
        'eveniet', 'ex', 'excepturi', 'exercitationem', 'expedita', 'explicabo',
        'facere', 'facilis', 'fuga', 'fugiat', 'fugit', 'harum', 'hic', 'id',
        'illo', 'illum', 'impedit', 'in', 'incidunt', 'inventore', 'ipsa',
        'ipsam', 'ipsum', 'iste', 'itaque', 'iure', 'iusto', 'labore',
        'laboriosam', 'laborum', 'laudantium', 'libero', 'magnam', 'magni',
        'maiores', 'maxime', 'minima', 'minus', 'modi', 'molestiae',
        'molestias', 'mollitia', 'nam', 'natus', 'necessitatibus', 'nemo',
        'neque', 'nesciunt', 'nihil', 'nisi', 'nobis', 'non', 'nostrum',
        'nulla', 'numquam', 'occaecati', 'odio', 'odit', 'officia', 'officiis',
        'omnis', 'optio', 'pariatur', 'perferendis', 'perspiciatis', 'placeat',
        'porro', 'possimus', 'praesentium', 'provident', 'quae', 'quaerat',
        'quam', 'quas', 'quasi', 'qui', 'quia', 'quibusdam', 'quidem', 'quis',
        'quisquam', 'quo', 'quod', 'quos', 'ratione', 'recusandae',
        'reiciendis', 'rem', 'repellat', 'repellendus', 'reprehenderit',
        'repudiandae', 'rerum', 'saepe', 'sapiente', 'sed', 'sequi',
        'similique', 'sint', 'sit', 'soluta', 'sunt', 'suscipit', 'tempora',
        'tempore', 'temporibus', 'tenetur', 'totam', 'ullam', 'unde', 'ut',
        'vel', 'velit', 'veniam', 'veritatis', 'vero', 'vitae', 'voluptas',
        'voluptate', 'voluptatem', 'voluptates', 'voluptatibus', 'voluptatum'
    )

    @classmethod
    def word(cls):
        return cls.random_element(cls.word_list)

    def arstext(self, phrase='arsenal', **kwargs):
        chance = kwargs.get('chance', random.randint(1, 100))
        text_length = kwargs.get('text_length', random.randint(5, 25))
        # random boolean, according to chance
        toss = random.randrange(100) < chance
        words = [self.word() for w in range(text_length)]
        if toss:
            words = self.replace_word(words, phrase)
        string = ' '.join(words)
        return string

    @staticmethod
    def replace_word(words, phrase):
        index = random.randint(0, len(words)-1)
        words[index] = phrase
        return words
