import unittest
import os.path
from db import DBHelper
from faker import Faker
from unittest.mock import Mock
from tinydb import Query

test_db_path = 'testdb.json'


class TestDatabaseClass(unittest.TestCase):
    def generate_fake_submission(self):
        return Mock(id=self.faker.numerify(), selftext=self.faker.text())

    def setup_method(self, test_method):
        self.db_helper = DBHelper(test_db_path)
        self.faker = Faker()

    def teardown_method(self, test_method):
        os.remove(test_db_path)

    def test_db_exists(self):
        self.assertTrue(hasattr(self, 'db_helper'))
        self.assertTrue(os.path.exists(test_db_path))

    def test_db_contains_contains_record(self):
        submission = self.generate_fake_submission()
        self.db_helper.db.insert({
            'id': submission.id,
            'selftext': submission.selftext
        })
        db_contains_mock = self.db_helper.contains_submission(submission)
        db_contains_mock_by_count = bool(self.db_helper.db.count(
            Query().id == submission.id
        ))

        self.assertTrue(db_contains_mock)
        self.assertEqual(db_contains_mock, db_contains_mock_by_count)

    def test_db_insertion(self):
        submission = self.generate_fake_submission()
        self.assertFalse(self.db_helper.contains_submission(submission))
        self.db_helper.insert_submission(submission)
        self.assertTrue(self.db_helper.contains_submission(submission))
