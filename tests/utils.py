from unittest.mock import Mock
from faker import Faker
from providers import CustomSubmissionProvider
from itertools import chain
import random
from uuid import uuid4

fake = Faker()
fake.add_provider(CustomSubmissionProvider)


def generate_fake_submission(**kwargs):
    sub = Mock()
    sub.selftext = fake.arstext(**kwargs)
    sub.id = str(uuid4())
    return sub


def generate_fake_submissions(n, **kwargs):
    for i in range(n):
        yield generate_fake_submission(**kwargs)


def get_submissions_without_phrase(n):
    return generate_fake_submissions(n, chance=0)


def get_submissions_with_phrase(n, m=1):
    subs = chain(
        generate_fake_submissions(m, chance=100),
        generate_fake_submissions(n-m, chance=0)
    )
    subs_list = list(subs)
    random.shuffle(subs_list)
    for sub in subs_list:
        yield sub
