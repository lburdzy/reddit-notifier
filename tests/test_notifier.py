import os
import types
import unittest
import utils
from unittest.mock import patch
from hypothesis import given, settings
from hypothesis.strategies import integers
from notifier import RedditNotifier, app_settings
from praw.models.listing.generator import ListingGenerator


class TestRedditClass(unittest.TestCase):
    @staticmethod
    def phrase_in_all_submissions(phrase, subs_list):
        for sub in subs_list:
            if not RedditNotifier.phrase_in_submission(phrase, sub):
                return False
        return True

    @staticmethod
    def phrase_in_no_submissions(phrase, subs_list):
        for sub in subs_list:
            if RedditNotifier.phrase_in_submission(phrase, sub):
                return False
        return True

    def setup_method(self, test_method):
        self.reddit = RedditNotifier(db_file_location=app_settings['TEST_DATABASE_FILE'])
        self.target_phrase = app_settings.get('TARGET_PHRASE')

    def teardown_method(self, test_method):
        os.remove(app_settings['TEST_DATABASE_FILE'])

    def test_submissions(self):
        submissions = self.reddit.get_submissions()
        self.assertTrue(submissions)

    def test_submissions_is_generator_object(self):
        submissions = self.reddit.get_submissions()
        self.assertIsInstance(submissions, ListingGenerator)

    @given(n=integers(min_value=20, max_value=100))
    @settings(max_examples=20)
    def test_filtering_submissions(self, n):
        subs = list(utils.generate_fake_submissions(n))
        filtered_subs = list(self.reddit.filter_submissions(
            subs, self.target_phrase
            ))
        reject_subs = [sub for sub in subs if sub not in filtered_subs]
        self.assertTrue(self.phrase_in_all_submissions(self.target_phrase, filtered_subs))
        self.assertTrue(self.phrase_in_no_submissions(self.target_phrase, reject_subs))

    def test_new_submissions_is_generator_object(self):
        submissions = utils.generate_fake_submissions(10)
        new_submissions = self.reddit.get_new_submissions(submissions)
        self.assertIsInstance(new_submissions, types.GeneratorType)

    def test_new_submissions_is_empty_if_phrase_is_not_present(self):
        submissions = utils.get_submissions_without_phrase(10)
        filtered_submissions = self.reddit.filter_submissions(submissions)
        new_submissions = self.reddit.get_new_submissions(filtered_submissions)
        self.assertTrue(len(list(new_submissions)) == 0)

    def test_new_submissions_is_full_if_phrase_is_present(self):
        submissions = list(utils.get_submissions_with_phrase(10, 10))
        filtered_submissions = self.reddit.filter_submissions(submissions)
        new_submissions = self.reddit.get_new_submissions(filtered_submissions)
        self.assertEqual(len(list(new_submissions)), len(submissions))

    def test_submissions_for_notifier_gets_correct_number_of_submissions(self):
        with patch('notifier.RedditNotifier.get_submissions') as m:
            m.return_value = utils.get_submissions_with_phrase(20, 15)
            notifier_submissions = list(self.reddit.get_submissions_for_notifier())
            self.assertEqual(len(notifier_submissions), 15)
