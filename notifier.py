import os
import praw
from collections import ChainMap
from db import DBHelper

defaults = {
    'REDDIT_USER_AGENT': 'myuniqueuseragentbc4b2efe',
    'SUBREDDIT_NAME': 'footballhighlights',
    'TARGET_PHRASE': 'arsenal',
    'DATABASE_FILE': 'databases/arsenal_submissions.json',
    'TEST_DATABASE_FILE': 'tests/temp_db.json',
}

app_settings = ChainMap(os.environ, defaults)


class RedditNotifier():
    def __init__(
        self,
        user_agent=app_settings['REDDIT_USER_AGENT'],
        client_id=app_settings['REDDIT_CLIENT_ID'],
        client_secret=app_settings['REDDIT_CLIENT_SECRET'],
        db_file_location=app_settings['DATABASE_FILE']
    ):
        self.reddit_obj = praw.Reddit(
            user_agent=user_agent,
            client_id=client_id,
            client_secret=client_secret,
        )
        self.db_helper = DBHelper(db_file_location)

    def get_submissions(self, subreddit_name=defaults['SUBREDDIT_NAME']):
        return self.reddit_obj.subreddit(subreddit_name).top(
            time_filter='week',
            limit=None,
        )

    def filter_submissions(self, submissions,
                           phrase=app_settings['TARGET_PHRASE']):
        for sub in submissions:
            if self.phrase_in_submission(phrase, sub):
                yield sub

    @staticmethod
    def phrase_in_submission(phrase, submission):
        return True if phrase.lower() in submission.selftext.lower() else False

    def get_new_submissions(self, submissions):
        for submission in submissions:
            if not self.db_helper.contains_submission(submission):
                self.db_helper.insert_submission(submission)
                yield submission

    def get_submissions_for_notifier(
        self, subreddit_name=app_settings['SUBREDDIT_NAME'],
        phrase=app_settings['TARGET_PHRASE']
    ):
        submissions = self.filter_submissions(
            self.get_submissions(subreddit_name),
            phrase=phrase,
        )
        return self.get_new_submissions(submissions)

    def notifier(self):
        pass

