from tinydb import TinyDB, Query


class DBHelper():
    def __init__(self, db_file_path):
        self.db = TinyDB(db_file_path)
        self.Submission = Query()

    def contains_submission(self, submission):
        return self.db.contains(self.Submission.id == submission.id)

    def insert_submission(self, submission):
        submission_dict = {
            'id': submission.id,
            'selftext': submission.selftext,
        }
        self.db.insert(submission_dict)
